package com.ubs.opsit.interviews;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by Naga Sarath on 1/26/15.
 */
public class BerlinClock implements TimeConverter{
    private static final String RED = "R";
    private static final String YELLOW = "Y";
    private static final String OFF = "O";
    private static final String MID_NIGHT = "24:00:00";
    private static final String INVALID_FORMAT = "INVALID_FORMAT";

    /**
     * this method builds the four block row for given length by color
     */
    private String buildFourBlockRowByColor(int length, String color){
        StringBuilder rowBuilder = new StringBuilder(4);
        int rowLength = 4;
        int offLength = rowLength - length;
        for (int index = 0; index < rowLength; index++){
            if (index < length){
                rowBuilder.append(color);
            } else {
                rowBuilder.append(OFF);
            }
        }

        return rowBuilder.toString();
    }
    /**
     * this method builds the eleven block row of Yellow with 15min Red blocks
     */
    private String buildElevenBlockRow(int length){
        StringBuilder rowBuilder = new StringBuilder(11);
        int rowLength = 11;
        int offLength = rowLength - length;
        for (int index = 0; index < rowLength; index++){
            if (index < length){
                rowBuilder.append((index+1)%3==0 ? RED : YELLOW);
            } else {
                rowBuilder.append(OFF);
            }
        }
        return rowBuilder.toString();
    }
    /**
     * this method returns seconds row output for given seconds
     */
    private String getTopSecondsRow(int seconds){
        return (seconds%2 == 0? YELLOW:OFF);
    }
    /**
     * this method returns first row of hours output for given hours
     */
    private String getTopHoursRow(int hours){
        int redLength = hours/5;
        return buildFourBlockRowByColor(redLength, RED);
    }
    /**
     * this method returns second row of hours output for given hours
     */
    private String getBottomHoursRow(int hours){
        int redLength = hours%5;
        return buildFourBlockRowByColor(redLength, RED);
    }
    /**
     * this method returns first row of minutes output for given minutes
     */
    private String getTopMinutesRow(int minutes){
        int yellowLength = minutes/5;
        return buildElevenBlockRow(yellowLength);
    }
    /**
     * this method returns second row of minutes output for given minutes
     */
    private String getBottomMinutesRow(int minutes){
        int yellowLength = minutes%5;
        return buildFourBlockRowByColor(yellowLength, YELLOW);
    }

    /**
     * this method returns Berlin Clock output for given time.
     */
    public String convertTime(String timeStr){
        String bcTimeFormat = null;
        DateFormat formatter = new SimpleDateFormat("hh:mm:ss");

        Date time = null;
        try {
            time = (Date) formatter.parse(timeStr);
        }catch(ParseException e){
            bcTimeFormat = INVALID_FORMAT;
        }

        if (time != null){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(time);
            // special case for mid night, as Java data object considers 24:00:00 as 00:00:00
            int hrs = timeStr.equals(MID_NIGHT) ? 24 : calendar.get(Calendar.HOUR_OF_DAY);
            int mins = calendar.get(Calendar.MINUTE);
            int secs = calendar.get(Calendar.SECOND);

            String secondsRow = getTopSecondsRow(secs);
            String topHrsRow = getTopHoursRow(hrs);
            String btmHrsRow = getBottomHoursRow(hrs);
            String topMinRow = getTopMinutesRow(mins);
            String btmMinRow = getBottomMinutesRow(mins);

            bcTimeFormat = String.format("%s\n%s\n%s\n%s\n%s",secondsRow, topHrsRow, btmHrsRow, topMinRow, btmMinRow);
        }

        return bcTimeFormat;
    }
}
